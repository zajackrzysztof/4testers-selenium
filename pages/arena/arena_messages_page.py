from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class ArenaMessagesPage:
    def __init__(self, browser):
        self.browser = browser

    def wait_for_text_area_load(self):
        WebDriverWait(self.browser, 10).until(expected_conditions.visibility_of_element_located
                                              ((By.CSS_SELECTOR, 'textarea')))

    def insert_message(self, random_text):
        self.browser.find_element(By.CSS_SELECTOR, 'textarea').send_keys(random_text)
