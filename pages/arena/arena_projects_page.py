from selenium.webdriver.common.by import By

from utils.random_message import generate_random_text


class ArenaProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_title(self, title_name):
        assert self.browser.find_element(By.CSS_SELECTOR, '.content_title').text == title_name

    def click_new_project_button(self):
        self.browser.find_element(By.LINK_TEXT, 'DODAJ PROJEKT').click()

    def insert_random_project_name(self, project_name):
        self.browser.find_element(By.CSS_SELECTOR, "#name").send_keys(project_name)

    def insert_random_project_prefix(self, number_of_characters):
        self.browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(generate_random_text(number_of_characters))

    def insert_random_project_description(self, number_of_characters):
        self.browser.find_element(By.CSS_SELECTOR, "#description").send_keys(generate_random_text(number_of_characters))

    def save_new_project(self):
        self.browser.find_element(By.CSS_SELECTOR, "input#save").click()

    def click_projects_button(self):
        self.browser.find_element(By.CSS_SELECTOR, ".activeMenu").click()
