from selenium.webdriver.common.by import By


class SearchProjectsPage:
    def __init__(self, browser):
        self.browser = browser

    def insert_text(self, input_text):
        self.browser.find_element(By.CSS_SELECTOR, "#search").send_keys(input_text)

    def click_search_button(self):
        self.browser.find_element(By.CSS_SELECTOR, "#j_searchButton").click()

    def confirm_search(self):
        assert self.browser.find_element(By.CSS_SELECTOR, "td a")
