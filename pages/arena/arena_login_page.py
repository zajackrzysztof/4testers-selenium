from selenium.webdriver.common.by import By


class ArenaLoginPage:
    def __init__(self, browser):
        self.browser = browser
        self.default_email = 'administrator@testarena.pl'
        self.default_password = 'sumXQQ72$L'

    def login(self):
        self.browser.find_element(By.ID, 'email').send_keys(self.default_email)
        self.browser.find_element(By.ID, 'password').send_keys(self.default_password)
        self.browser.find_element(By.ID, 'login').click()
