from selenium.webdriver.common.by import By


class HomePage:
    def __init__(self, browser):
        self.browser = browser

    def verify_post_count(self, expected_count):
        title_post_list = self.browser.find_elements(By.CSS_SELECTOR, '.post-title a')
        assert len(title_post_list) == expected_count

    def search_for(self, search_text):
        search_input = self.browser.find_element(By.CSS_SELECTOR, '.gsc-input[title=search]')
        search_button = self.browser.find_element(By.CSS_SELECTOR, '.gsc-search-button[title=search]')
        search_input.send_keys(search_text)
        search_button.click()

    def click_label(self, label_name):
        self.browser.find_element(By.LINK_TEXT, label_name).click()
