import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


@pytest.fixture()
def browser():
    driver = Chrome(executable_path=ChromeDriverManager().install())
    driver.get('https://awesome-testing.blogspot.com/')
    yield driver
    driver.quit()


def test_post_count(browser):
    title_post_list = browser.find_elements(By.CSS_SELECTOR, '.post-title a')
    assert len(title_post_list) == 4


def test_post_count_after_search(browser):
    search_input = browser.find_element(By.CSS_SELECTOR, '.gsc-input[title=search]')
    search_button = browser.find_element(By.CSS_SELECTOR, '.gsc-search-button[title=search]')
    search_input.send_keys('Selenium')
    search_button.click()
    wait = WebDriverWait(browser, 10)
    element_for_wait_for = (By.CSS_SELECTOR, '.status-msg-body')
    wait.until(expected_conditions.visibility_of_element_located(element_for_wait_for))
    title_list = browser.find_elements(By.CSS_SELECTOR, 'h1 a')
    assert len(title_list) == 20


def test_post_count_on_cypress_label(browser):
    link = browser.find_element(By.LINK_TEXT, '2016')
    link.click()
    post_title_list = browser.find_elements(By.CSS_SELECTOR, '.post-title a')
    assert len(post_title_list) == 24
