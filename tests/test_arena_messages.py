import time
import pytest
import selenium.webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager
from pages.arena.arena_messages_page import ArenaMessagesPage
from utils.random_message import generate_random_text


@pytest.fixture()
def browser():
    driver = selenium.webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    driver.find_element(By.ID, 'email').send_keys('administrator@testarena.pl')
    driver.find_element(By.ID, 'password').send_keys('sumXQQ72$L')
    driver.find_element(By.ID, 'login').click()
    driver.find_element(By.CSS_SELECTOR, '.icon_mail').click()
    WebDriverWait(driver, 10).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, 'textarea')))
    yield driver
    driver.quit()


def test_should_add_new_message(browser):
    browser.find_element(By.CSS_SELECTOR, 'textarea').send_keys(generate_random_text(10))
    browser.find_element(By.CSS_SELECTOR, '.j_msgResponse').click()


def test_should_add_new_message2(browser):
    random_text = generate_random_text(10)
    arena_messages_page = ArenaMessagesPage(browser)
    arena_messages_page.wait_for_text_area_load()
    arena_messages_page.insert_message(random_text)
    time.sleep(2)
