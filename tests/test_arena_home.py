import pytest
import selenium.webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login_page import ArenaLoginPage
from pages.arena.arena_messages_page import ArenaMessagesPage
from pages.arena.arena_projects_page import ArenaProjectsPage


@pytest.fixture()
def browser():
    driver = selenium.webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    ArenaLoginPage(driver).login()
    yield driver
    driver.quit()


def test_should_display_email_in_user_section(browser):
    ArenaHomePage(browser).verify_displayed_email('administrator@testarena.pl')


def test_should_successfully_logout(browser):
    ArenaHomePage(browser).logout()
    assert browser.current_url == 'http://demo.testarena.pl/zaloguj'


def test_should_open_messages_and_display_text_area(browser):
    ArenaHomePage(browser).click_mail()
    ArenaMessagesPage(browser).wait_for_text_area_load()


def test_should_open_projects_page(browser):
    ArenaHomePage(browser).click_icon_tools()
    ArenaProjectsPage(browser).verify_title('Projekty')
