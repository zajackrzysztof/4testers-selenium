import pytest
import selenium.webdriver
from webdriver_manager.chrome import ChromeDriverManager
from pages.arena.arena_home_page import ArenaHomePage
from pages.arena.arena_login_page import ArenaLoginPage
from pages.arena.arena_projects_page import ArenaProjectsPage
from pages.arena.arena_search_projects_page import SearchProjectsPage
from utils.random_message import generate_random_text


@pytest.fixture()
def browser():
    driver = selenium.webdriver.Chrome(executable_path=ChromeDriverManager().install())
    driver.get('http://demo.testarena.pl/zaloguj')
    ArenaLoginPage(driver).login()
    yield driver
    driver.quit()


def test_should_add_new_project_to_test_arena(browser):
    ArenaHomePage(browser).click_icon_tools()

    ArenaProjectsPage(browser).verify_title('Projekty')
    ArenaProjectsPage(browser).click_new_project_button()
    project_name = generate_random_text(10)
    ArenaProjectsPage(browser).insert_random_project_name(project_name)
    ArenaProjectsPage(browser).insert_random_project_prefix(4)
    ArenaProjectsPage(browser).insert_random_project_description(20)
    ArenaProjectsPage(browser).save_new_project()
    ArenaProjectsPage(browser).click_projects_button()

    SearchProjectsPage(browser).insert_text(project_name)
    SearchProjectsPage(browser).click_search_button()
    SearchProjectsPage(browser).confirm_search()
