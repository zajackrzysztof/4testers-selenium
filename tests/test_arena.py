from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


# Test - Logowanie do test areny
def test_login_to_testarena():
    # Uruchomienie przeglądarki Chrome. Ścieżka do chromedrivera
    # ustawiana automatycznie przez bibliotekę webdriver-manager
    browser = Chrome(executable_path=ChromeDriverManager().install())

    # Otwarcie strony testareny - pierwsze użycie Selenium API
    browser.get('http://demo.testarena.pl/zaloguj')

    # Zalogowanie się
    email_field = browser.find_element(By.CSS_SELECTOR, '#email')
    password_field = browser.find_element(By.CSS_SELECTOR, '#password')
    login_button = browser.find_element(By.CSS_SELECTOR, '#login')

    assert email_field.is_displayed()
    assert password_field.is_displayed()
    assert login_button.is_displayed()

    email_field.send_keys('administrator@testarena.pl')
    password_field.send_keys('sumXQQ72$L')
    login_button.click()

    user_info = browser.find_element(By.CSS_SELECTOR, '.user-info small')
    assert user_info.text == 'administrator@testarena.pl'

    # Zamknięcie przeglądarki
    browser.quit()
